import {combineReducers} from 'redux'
import { reducer as formReducer } from 'redux-form'
import common from "common/reducer";
import board from "board/reducer";

const rootReducer = combineReducers({
  form: formReducer,
  common,
  board
});

export default rootReducer;
