import React, { PureComponent } from 'react';
import Button from 'react-md/lib/Buttons/Button';
import Card from 'react-md/lib/Cards/Card';
import CardTitle from 'react-md/lib/Cards/CardTitle';
import CardText from 'react-md/lib/Cards/CardText';
import CardActions from 'react-md/lib/Cards/CardActions';
import List from 'react-md/lib/Lists/List';
import ListItem from 'react-md/lib/Lists/ListItem';

export default class Setting extends PureComponent {
  render() {
    return (
      <Card className="board">
        <CardTitle title="Setting" />
        <CardText>
          <h2 className="md-display-4 display-override">Setting title</h2>
        </CardText>
        <List ordered>
          <ListItem primaryText="List 1" />
          <ListItem primaryText="List 2" />
          <ListItem primaryText="List 3" />
        </List>
        <CardActions className="md-divider-border md-divider-border--top">
          <Button flat label="View" secondary />
        </CardActions>
      </Card>
    );
  }
}