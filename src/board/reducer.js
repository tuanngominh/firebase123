import { has } from "lodash";

//Reducer
const initialState = {
  boards: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_BOARDS":
      return {
        ...state,
        boards: action.boards
      }
    case "FETCH_LABELS":
      return {
        ...state,
        labels: action.labels
      }
    case "FETCH_TAGS":
      return {
        ...state,
        tags: action.tags
      }
    default:
      return state;
  }
}

export default reducer;

//Selectors
export function getBoards(state) {
  return has(state, ["board", "boards"]) ? state.board.boards : [];
}

export function getLabels(state) {
  return has(state, ["board", "labels"]) ? state.board.labels : [];
}

export function getTags(state) {
  return has(state, ["board", "tags"]) ? state.board.tags : [];
}