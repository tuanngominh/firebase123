import * as firebase from 'firebase';
import history from "common/history";

export const fetchBoards = () => {
  return async (dispatch) => {
    const snapshot = await firebase.database().ref("boards").once("value");
    let boards = snapshot.val();
    boards = Object.keys(boards).map(key => ({
      ...boards[key],
      key
    }));
    dispatch({
      type: "FETCH_BOARDS",
      boards
    });
  }
}

export const fetchLabels = () => {
  return async (dispatch) => {
    const snapshot = await firebase.database().ref("labels").once("value");
    let labels = snapshot.val();
    labels = Object.keys(labels).map(key => ({
      ...labels[key],
      key
    }));
    dispatch({
      type: "FETCH_LABELS",
      labels
    });
  }
}

export const fetchTags = () => {
  return async (dispatch) => {
    const snapshot = await firebase.database().ref("tags").once("value");
    let tags = snapshot.val();
    tags = Object.keys(tags).map(key => ({
      ...tags[key],
      key
    }));
    dispatch({
      type: "FETCH_TAGS",
      tags
    });
  }
}

export const addBoard = (data) => {
  return async (dispatch) => {
    const { name } = data;
    try {
      await firebase.database().ref("boards").push({
        name
      });
      history.push("/boards");
    } catch (error) {
      dispatch({type: "SET_MESSAGE", message: error.message});
    }
  }
}