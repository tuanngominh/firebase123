import React, { PureComponent } from 'react';
import { Route, Switch } from 'react-router-dom'
import BoardList from "./BoardList";
import AddBoard from "./AddBoard";

export default class Board extends PureComponent {
  render() {
    return (
      <Switch>
        <Route path="/boards/add" component={AddBoard} />
        <Route exact path="/boards" component={BoardList} />
      </Switch>
    );
  }
}