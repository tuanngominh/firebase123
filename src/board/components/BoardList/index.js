import { connect } from "react-redux";
import { getBoards } from "board/reducer";
import { fetchBoards } from "board/actions";
import BoardList from "./BoardList";

const enhance = connect(
  state => ({
    boards: getBoards(state)
  }),
  dispatch => ({
    fetchBoards: () => {
      dispatch(fetchBoards());
    }
  })
);

export default enhance(BoardList);
