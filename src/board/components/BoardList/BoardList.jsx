import React, { PureComponent } from 'react';
import { Button, Card, CardTitle, CardText, List, ListItem } from 'react-md';

export default class BoardList extends PureComponent {
  componentWillMount() {
    const { fetchBoards } = this.props;
    fetchBoards();
  }

  render() {
    const { boards } = this.props;
    return (
      <Card
        className="md-cell md-cell--6 md-cell--3-desktop-offset md-cell--1-tablet-offset"
      >
        <CardTitle title="Boards" />
        { boards.length > 0
        ? <List ordered>
            {boards.map(board => <ListItem primaryText={board.name} key={board.key} />)}
          </List>
        : <CardText style={{textAlign: "center"}}>
          There is no board, let add one.<br/><br/>
          <Button raised primary label="Add Board" onClick={() => this.props.history.push("/boards/add")}/>
        </CardText>
        }
      </Card>
    );
  }
}
