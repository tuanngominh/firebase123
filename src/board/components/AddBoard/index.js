import { reduxForm } from "redux-form";
import { compose } from "redux";
import { connect } from "react-redux";
import { addBoard, fetchTags} from "board/actions";
import { getTags } from "board/reducer";
import AddBoard from "./AddBoard";

const enhance = compose(
  connect(
    state => ({
      tags: getTags(state)
    }),
    dispatch => ({
      fetchTags: () => dispatch(fetchTags())
    })
  ),
  reduxForm({
    onSubmit: (data, dispatch, props) => {
      dispatch(addBoard(data));
    },
    form: 'add-board'
  })
);;

export default enhance(AddBoard);
