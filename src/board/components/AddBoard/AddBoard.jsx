import React, { PureComponent } from 'react';
import { Button, Card, CardTitle, TextField} from 'react-md';
import { Field } from 'redux-form';
import FieldPropFilter from "common/components/FieldPropFilter";
import validators from "common/validators";

export default class AddBoard extends PureComponent {
  componentWillMount() {
    this.props.fetchTags();
  }

  render() {
    const { handleSubmit, tags } = this.props;
    console.log(tags);
    return (
      <form
        onSubmit={handleSubmit}
        className="md-cell md-cell--6 md-cell--3-desktop-offset md-cell--1-tablet-offset"
      >
        <Card className="md-grid">
          <CardTitle title="Add Board" />
          <Field
            name="name"
            type="text"
            validate={[validators.required]}
            component={FieldPropFilter}
            finalComponent={TextField}
            id="name"
            label="name"
            lineDirection="center"
            placeholder="Enter board name"
            className="md-cell md-cell--12 md-cell--bottom"
          />
          <Button type="submit" raised primary label="Save" onClick={handleSubmit} />
        </Card>
      </form>
    );
  }
}
