import * as firebase from 'firebase';

export const login = (email, password) => {
  return async (dispatch) => {
    try {
      await firebase.auth().signInWithEmailAndPassword(email, password);
    } catch (error) {
      dispatch({type: "SET_MESSAGE", message: error.message});
    }
  }
}