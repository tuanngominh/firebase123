import { connect } from "react-redux";
import Message from "./Message";

export default connect(
  state => ({
    message: state.common.message
  }),
  dispatch => ({
    onClearMessage: () => dispatch({type: "CLEAR_MESSAGE"})
  })
)(Message);
