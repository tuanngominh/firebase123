import React from "react";
import Snackbar from "react-md/lib/Snackbars";

const Message = ({ onClearMessage, message }) => (
  <Snackbar
    toasts={message ? [{ text: message }] : []}
    autohide
    onDismiss={onClearMessage}
  />
);

export default Message;
