import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from 'react-router-dom'
import { isAuthenticated, isAuthenticationDone } from "common/reducer";

const PrivateRoute = ({ component: Component, isAuthenticated, ...rest }) => {
  return (
    <Route {...rest} render={props => (
      isAuthenticationDone
      ? isAuthenticated
        ? <Component {...props}/>
        : <Redirect to={{
          pathname: "/login",
          state: {from : props.location}
        }} />
      : null
      )} />
  )};

// export default PrivateRoute;
export default connect(
  state => ({
    isAuthenticated: isAuthenticated(state)
  })
)(PrivateRoute);
