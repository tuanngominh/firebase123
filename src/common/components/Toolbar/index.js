import { connect } from "react-redux";
import { isAuthenticated } from "common/reducer";
import Toolbar from "./Toolbar";

export default connect(
  state => ({
    isAuthenticated: isAuthenticated(state)
  }),
  dispatch => ({
    showMessage: message => dispatch({type: "SET_MESSAGE", message: message})
  })
)(Toolbar);
