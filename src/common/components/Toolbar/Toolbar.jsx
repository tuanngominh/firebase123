import React from "react";
import Toolbar from "react-md/lib/Toolbars";
import { Button } from "react-md";
import * as firebase from 'firebase';

const LinkButton = ({ to, label, history}) => <Button flat onClick={() => {
    history.push(to);
  }}
  label={label}
/>;

const LinkIconButton = ({ children, to, history}) => <Button className="md-btn--toolbar" icon onClick={() => {
    history.push(to);
  }}
>{children}</Button>;

const MyToolbar = ({ isAuthenticated, history, showMessage }) => <Toolbar
  colored
  title="Firebase + React sample app"
  actions={isAuthenticated
    ? [
      <LinkIconButton to="/boards/add" history={history} >add_box</LinkIconButton>,
      <LinkButton to="/boards" label="Boards" history={history} />,
      <LinkButton to="/settings" label="Settings" history={history} />,
      <Button icon onClick={() => {
          firebase.auth().signOut().then(function() {
            history.push("/login");
          }).catch(function(error) {
            showMessage(error.message);
          });
        }}
      >power_settings_new</Button>
    ]
    : [
      <LinkButton to="/login" label="Login" history={history} />
    ]
    }
/>;

export default MyToolbar;
