import React, { PureComponent } from 'react';
import { Button, Card, CardTitle, TextField} from 'react-md';
import { Redirect } from "react-router-dom";
import { Field } from 'redux-form';
import FieldPropFilter from "common/components/FieldPropFilter";
import validators from "common/validators";

class LoginForm extends PureComponent {
  render() {
    const { from } = this.props.location.state || { from: { pathname: '/boards' } }
    if (this.props.isAuthenticated) {
      return (
        <Redirect to={from}/>
      )
    }

    return (
      <form  onSubmit={this.props.handleSubmit}  className="md-cell md-cell--6 md-cell--3-desktop-offset md-cell--1-tablet-offset">
        <Card className="md-grid">
            <CardTitle title="Login" />
            <Field
              name="email"
              type="text"
              validate={[validators.required, validators.email]}
              component={FieldPropFilter}
              finalComponent={TextField}
              id="email"
              label="Email"
              lineDirection="center"
              placeholder="Enter your email"
              className="md-cell md-cell--12 md-cell--bottom"
            />
            <Field
              name="password"
              type="password"
              validate={validators.required}
              component={FieldPropFilter}
              finalComponent={TextField}
              id="password"
              label="Password"
              lineDirection="center"
              placeholder="Enter your password"
              className="md-cell md-cell--12 md-cell--bottom"
            />
            <Button type="submit" raised primary label="Login" onClick={this.props.handleSubmit} />
        </Card>
      </form>
    );
  }
}

export default LoginForm;
