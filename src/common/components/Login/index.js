import { compose } from "redux";
import { connect } from "react-redux";
import { login } from "common/actions";
import { reduxForm } from 'redux-form';
import { isAuthenticated } from "common/reducer";
import LoginForm from "./Login";

const enhance = compose(
  connect(
    state => ({
      isAuthenticated: isAuthenticated(state)
    })
  ),
  reduxForm({
    onSubmit: (data, dispatch, props) => {
      const { email, password } = data;
      dispatch(login(email, password));
    },
    form: 'login'
  })
);

export default enhance(LoginForm);