import React from "react";

//Remove input and meta props passed from redux-form
const FieldPropFilter = ({
  input,
  meta: {touched, error, warning},
  finalComponent: FinalComponent,
  ...rest
}) => (
  <FinalComponent
    {...rest}
    {...input}
    error={(touched && (error || warning)) ? true : false}
    errorText={touched && (error || warning) ? error || warning : null}
  />
)

export default FieldPropFilter;