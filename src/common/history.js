// To be able to call history.push outside Router
// @see:
// https://github.com/ReactTraining/react-router/issues/3498#issuecomment-301057248
// https://github.com/ReactTraining/react-router/issues/4059#issuecomment-306506430

import createHistory from 'history/createBrowserHistory'

export default createHistory();
