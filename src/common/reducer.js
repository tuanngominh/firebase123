const initialState = {
  message: null,
  user: false,
};

const common = (state = initialState, action) => {
  switch (action.type) {
    case "SET_USER":
      return {
        ...state,
        user: action.user
      }
    case "CLEAR_USER":
      return {
        ...state,
        user: null
      }
    case "CLEAR_MESSAGE":
      return {
        ...state,
        message: null
      }
    case "SET_MESSAGE":
      return {
        ...state,
        message: action.message
      }
    default:
      return state;
  }
}

export default common;

export function isAuthenticated(state) {
  return state.common.user !== false && state.common.user !== null;
}

export function isAuthenticationDone(state) {
  return state.common.user !== false;
}
