import React, { Component } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import history from 'common/history';
import { connect } from "react-redux";
import * as firebase from 'firebase';
import PrivateRoute from "common/components/PrivateRoute";
import Board from "board";
import Setting from "setting/Setting";
import Login from "common/components/Login";
import Message from "common/components/Message";
import Toolbar from "common/components/Toolbar";
import { isAuthenticationDone } from "common/reducer";
import './App.css';

class App extends Component {

  componentDidMount() {
    // Globally listen for authentication changes
    firebase.auth().onAuthStateChanged(user => {
      if (!user && window.location.pathname !== "/") {
        this.props.clearUser();
      } else {
        this.props.setUser(user);
      }
    });
  }

  render() {
    const { isAuthenticationDone } = this.props;
    return (
      <Router history={history} >
        <div>
          <Route path="/" render={props => (
            isAuthenticationDone ? <Toolbar {...props} /> : null
          )} />
          <div className="md-grid">
            <Switch>
              <Route exact path="/login" render={props => (
                isAuthenticationDone
                ? <Login {...props} />
                : null
              )} />
              <PrivateRoute path="/boards" component={Board} />
              <PrivateRoute path="/settings" component={Setting} />
            </Switch>
          </div>
          <Message />
        </div>
      </Router>
    );
  }
}

export default connect(
  state => ({
    isAuthenticationDone: isAuthenticationDone(state)
  }),
  dispatch => ({
    setUser: user => {
      dispatch({type: 'SET_USER', user});
    },
    clearUser: () => {
      dispatch({type: 'CLEAR_USER'});
    }
  })
)(App);
