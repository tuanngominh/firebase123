import dotenv from "dotenv";
import admin from "firebase-admin";
import faker from "faker";

// Fetch the service account key JSON file contents
// As we use dynamic path so `require` instead of `import`
dotenv.config({path: ".env." + process.env.NODE_ENV});
const serviceAccount = require(process.env.FIREBASE_ADMIN_SDK);


import boardData from "./data/boards.json";
import labels from "./data/labels.json";

// Initialize the app with a service account, granting admin privileges
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.DATABASE_URL
});

// As an admin, the app has access to read and write all data, regardless of Security Rules
const db = admin.database();

async function setUpBoard(board) {
  console.log(`Setup board: ${board.name}`);
  var boardRef = await db.ref("boards").push({
      name: board.name
  })

  if (board.lists && board.lists.length > 0) {
    const listRefs = await Promise.all(board.lists.map(list => {
      return db.ref(`lists/${boardRef.key}`).push({
        name: list.name
      });
    }));

    const cardRefs = await Promise.all(board.lists.map((list, i) => {
      if (list.cards && list.cards.length > 0) {
        return Promise.all(list.cards.map(card => db.ref(`cards/${listRefs[i].key}`).push({
          name: card.name
        })));
      }
    }));
    return cardRefs;
  } else {
    return boardRef;
  }
}

async function setUpTag() {
  const tagCount = 1000;
  console.log(`Setup tag ${tagCount}`);
  const promises = [];
  let i = 0;

  while ( i < tagCount) {
    promises.push(db.ref(`tags`).push({
      name: faker.name.jobTitle()
    }));
    i++;
  }
  return Promise.all(promises);
}

async function importData() {
  //reset
  console.log("Reset db");
  await db.ref("boards").remove();
  await db.ref("lists").remove();
  await db.ref("cards").remove();
  await db.ref("tags").remove();
  await db.ref("labels").remove();

  await Promise.all(boardData.map(board => setUpBoard(board)));

  await setUpTag();

  console.log("Setup labels");
  return await db.ref("labels").set(labels);
}

importData().then(() => {
  process.exit();
})
.catch(error => {
  console.log(error);
  process.exit();
})
