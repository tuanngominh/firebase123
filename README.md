# Setup firebase admin
Setup firebase service account. Go to https://console.firebase.google.com/u/0/project/project-slug/settings/serviceaccounts/adminsdk to generate private key

We need firebase admin for importing data to firebase's database.

# Firebase database import
Import data from json to firebase's database
```sh
npm run import-development
npm run import-production
```

No need to change firebase project alias (`firebase use production`) which is used only for deployment.

# Firebase deploy envs: TEST and PRO
https://firebase.googleblog.com/2016/07/deploy-to-multiple-environments-with.html

```json
{
  "projects": {
    "default": "tuanngo-firebase-test-env",
    "production": "tuanngo-firebase-pro-env"
  }
}
```

Switch envs
```sh
firebase use default # sets environment to the default alias
firebase use production # sets environment to the staging alias
```

# Firebase functions
Deploy firebase functions
```sh
firebase deploy --only functions
# OR deploy everything to firebase
npm run deploy
```

# Firebase web client envs
Using create-react-app dotenv 
https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#adding-custom-environment-variables

# ES6
Motivation: if we can write as much ES6 as possible on firebase cloud function, firebase admin LIKE we do on react clientside THEN we can be more productive.

Inspiration: 
- Google example using typescript transpile to js https://youtu.be/GNR9El3XWYo?t=793
- Step Up Labs team example https://medium.com/step-up-labs/our-experience-with-cloud-functions-for-firebase-d206448a8850
- Guide: https://medium.com/@jthegedus/es6-in-cloud-functions-for-firebase-959b35e31cb0

At this moment (June 2, 2017), cloud function runtime is nodejs v6.9.1 which support many (95%) of ES6 spec (http://node.green/). But still missing important thing like : module (`import/export`) and `async/await`.

We use https://github.com/babel/babel-preset-env, and compile to node 6.9.1 to enable those additional feature like module, async/await. ES6 features which already support by that node version will not be transpiled.

Later when firebase upgrade its node version to 7.0 ( http://node.green/#ES2017-features-async-functions ), babel will no longer generate `asyn/await` transpiled code.

# Support absolute path for module path
`import moduleName/component/ComponentA` instead of 
`import ../../../moduleName/component/ComponentA`

Implementation reference https://github.com/facebookincubator/create-react-app/issues/741#issuecomment-278945308
